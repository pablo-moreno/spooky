from string import digits, ascii_letters
from random import choice


def generate_random_string(num_chars=16):
    return ''.join([choice(digits + ascii_letters) for _ in range(0, num_chars)])
