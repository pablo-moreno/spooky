from django.db import models
from django.utils.text import slugify
from uuid import uuid4


class BaseItem(models.Model):
    name = models.CharField(max_length=140)
    description = models.CharField(max_length=480)
    slug = models.SlugField(max_length=200)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    class Meta:
        abstract = True


class Deletable(models.Model):
    deleted = models.BooleanField(default=False)

    def delete(self, *args, **kwargs):
        self.deleted = True
        return self.save()

    class Meta:
        abstract = True


class WithUID(models.Model):
    uuid = models.UUIDField(default=uuid4)

    class Meta:
        abstract = True
