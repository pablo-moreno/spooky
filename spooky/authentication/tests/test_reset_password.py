from rest_framework.test import APITestCase
from rest_framework import status
from django.contrib.auth.models import User
from django.urls import reverse


class TestResetPassword(APITestCase):
    def setUp(self):
        self.username = 'darth.vader'
        self.user = User.objects.create_user(
            username=self.username,
            password='thisistheoldpassword',
            email=f'{self.username}@starwars.com'
        )

    def test_reset_password(self):
        data = {
            'email': self.user.email,
        }

        response = self.client.post(reverse('authentication:reset-password'), data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
