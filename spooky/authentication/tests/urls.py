from django.urls import path, include

urlpatterns = [
    path('api/auth/', include('spooky.authentication.urls', namespace='authentication')),
]
