# Spooky Apps

This is a set of Django utils apps

## Spooky Authentication

App with authentication endpoints.
Allows to create new Django users, change and reset user's password 
and to obtain JWT tokens.

### Settings

Required settings:

```python
INSTALLED_APPS = [
    # ...
    'rest_framework',
    'rest_framework.authtoken',
    'spooky.authentication',
]


REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        # ...
    ],
    # ...
}

JWT_AUTH = {
    'JWT_SECRET_KEY': SECRET_KEY,
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=7),
    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),
    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'spooky.authentication.jwt.handler',
}
```

Available app settings and its default values

```python
AUTH_SIGN_UP_BY_IP_LIMIT=10
MAX_LOGIN_RETRIES=10
EMAIL_RESET_PASSWORD_TEMPLATE_NAME='spooky_registration/password_reset_email.txt'
HTML_EMAIL_RESET_PASSWORD_TEMPLATE_NAME='spooky_registration/password_reset_email.html'
SUBJECT_EMAIL_RESET_PASSWORD_NAME='spooky_registration/password_reset_subject.txt'
```
